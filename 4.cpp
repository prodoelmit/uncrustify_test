#include <stdio.h>

#define min(a, b)  ((a) < (b) ? (a) : (b))

template<class T>
class list
{
};

class Bar
{
};

struct FooBase
{
};

int doSomething(...);

int doSomethingElse(...)
{
	return 2;
}

struct Foo : private FooBase
{
public:
	int i;

	virtual int action(int, char, float) = 0;

	virtual Foo* getSelf()
	{
		return this;
	}

private:
	static int method()
	{
	}

	list<Bar> bar;
};

namespace fooNS
{
	class FooClass : Foo,
					 virtual FooBase
	{
		typedef int (FooClass::* ACTION)(int);

	public:
		FooClass()
		{
			act = &FooClass::nv_action;
		}

		virtual ~FooClass()
		{
		}

		int nv_action(int arg)
		{
			return arg;
		}

		virtual int action(int color, char alpha, float);

		virtual Foo* getSelf()
		{
			return Foo::getSelf();
		}

		int method()
		{
			return 0;
		}

		ACTION act;
	private:
		int var;
	};
}

int fooNS::FooClass::action(int color, char alpha, float)
{
	return doSomething(color);
}

typedef void (fn)(int i, int j, int k);

typedef void (* block)(int i, int j, int k);

typedef int X;

int& refTest(X&& x)
{
	int**& p = (int**&)x;
	int static& r = *&x;
	return r && (r & x) ? r : x;
}

// todo something
void doSomething(int y, int b, void*(*)())
{
	int a = 1 || 0 && 1;
	int bb = a == !1 && a != 0;
	int ccc = bb = a = 1 < 2 >= 3;
	int dddd = ccc = bb = a = ~1 | 2 & 3 ^ 4;

	void* p1 = reinterpret_cast<void*>(&a);
	void** p2 = &p1;

	a = bb = ccc = dddd = 2;
	dddd = ccc = bb = a = (1 + 2 + 3 + 4 + 5 + 0xFFFFFFFFF);

	int i5 = ((1) + 2) - (4 * 5 / 6 % 7);
	int i6 = -1 << 2 >> -3;
	int i7 = 2 > 3 ? 7 + 8 + 9 : 11 + 12 + 13;
	int i8 = 2 < 3 + 7 + 8 + 9 ? : 11 + 12 + 13;
	int ii[6], jj[] = {
		1, 2, 3,
		0x000A, 0x000B, 0x000C
	};

	fooNS::FooClass object,
		* ptr = (fooNS::FooClass*)object.getSelf()->getSelf()->getSelf();
	(object.*object.act)(1);
	ptr->action(0xFF0000, 0.01, 320);
	ptr->getSelf()->getSelf()->getSelf()->getSelf();

	doSomething(ii[1], jj[ii[2]], doSomething(123));

	if (1) {doSomething(1);}
	else if (2)
	{
		doSomething(1, 2);
	}
	if (1)
	{
		doSomething();
	}
	else if (2)
	{
		doSomething();
	}
	else { doSomething();}
	for (int i = 1, j = 2; i <= j; i++, j--)
	{
		doSomethingElse();
	}
	while (1)
		doSomethingElse();
	do
	{
		doSomethingElse();
	}
	while (1);
	switch (1)
	{
	case 0:
		return;
	case 1:
	{
		return;
	}
	}

	try
	{
		doSomethingElse();
	}
	catch (char* message)
	{
		return;
	}
}

struct fooS
{
	int i;
	char j;
} foo_t;
enum fooE
{
	SUNDAY = 111, MONDAY = 222, TUESDAY = 333, WEDNESDAY = TUESDAY + 1
} foo_e;

template<typename T, typename M>
inline T const& Min(T const& a, M const& b)
{
	return a < b ? a : b;
}

template<typename K, typename V = list<K> >
class hash
{
};

template<class T>
struct FooT
{
	hash<int, list<char> > elems;

	template<int N>
	int foo()
	{
		return N;
	}

	template<>
	int foo<2>()
	{
		return Min<>(1, 5);
	}

	list<int> mem = {1, 2, 3};
	float vector[3];

	FooT()  :
		elems{{-1, {'c', 'p', 'p'}},
			{1, {'j', 'b'}}},
		vector {1f, 2f, 3f} {
		auto la = [=](int i1, int i2) -> bool mutable {
				return i1 < i2;
			} (1, 2);
	}

	auto f(T t)->decltype(t + doSomething())
	{
		return t + doSomething();
	}
};
