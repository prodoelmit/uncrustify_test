#!/bin/bash
cd "${0%/*}"

for f in *.cpp; do
	uncrustify -c .uncrustify.cfg --replace --no-backup $f
done
