#include <stdio.h>

void globalFunc();

namespace foo
{
	class Foo
	{
	public:
		Foo();

		~Foo();

		virtual Foo* getSelf()
		{
			return Foo::getSelf();
		}

	private:
		void innerFunc();

		int var;
	};
}
struct FooPOD
{
	int i;
};
struct FooC
{
private:
	int i;
};
extern int a;

static int innerFunc();

int a = innerFunc();

int innerFunc()
{
	return 5;
}

void foo::Foo::innerFunc()
{
	int continuation = 0xCD
		+ 0xFD + 0xBAADF00D + 0xDEADBEEF;
	auto la = [](int i1, int i2) -> bool mutable {
			return i1 < i2;
		} (1, 2);
}
